# X8 Collect Data #
Scripts and ros components to collect telemetry and camera data from the X8's on-board Odroid. Information is logged in a `.bag` file which can be retrieved after landing.

### How do I get set up? ###
* Install all ros dependencies (including mavros, pointgrey camera drivers, ...)
* Clone repository into ros workspace
* Create folder rosbags in the project directory
* Modify `scripts/startup.sh` with the right project location
* Add the following lines (change project location) to a new `.conf` file in `~/.config/upstart`:

```
#!bash

start on startup
task
exec nohup bash ~/ros_ws/sandbox/x8/scripts/startup.sh >> /dev/null 2>> /dev/null
```

* Install acpid
* Backup `/etc/acpi/powerbtn.sh` and replace it with:
```
#!/bin/sh
# /etc/acpi/powerbtn.sh
# Initiates a shutdown when the power putton has been
# pressed.
pkill roslaunch
sleep 10
pkill roscore
sleep 2
shutdown -h now
```
### How do I run it? ###
All ros processes will start on start up, and stop when you press the power button on the odroid after 10seconds (The fan will stop). You can then unplug the battery.
All packages are logged in a `.bag` file in `<path_to_project>/rosbags/dumpXXXXX.bag`.